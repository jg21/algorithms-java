package com.goodshepherd.utilties;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeMap;

import com.goodshepherd.datastructures.MSTGraph;
import com.goodshepherd.datastructures.UnionFind;

public class HammingDistanceUtility {
	
	public static HashMap<Integer,Set<int[]>> readHammingDistanceFromFile(String fileName,int maxDistanceNeeded) {
	
		HashMap<Integer,Set<int[]>> distanceToEdgesMapping = new HashMap<>();
		distanceToEdgesMapping.put(0, new HashSet<int[]>());
		HashMap<String,Integer> nodeBitsToNodeMappping = new HashMap<>();
		try (BufferedReader br = new BufferedReader(new FileReader(new File(fileName)))) {
			String lineRead = br.readLine();
			StringTokenizer st = new StringTokenizer(lineRead, " ");
			int numberOfNodes = Integer.parseInt(st.nextToken());
			//String[] nodeBitStrings = new String[numberOfNodes];
			UnionFind unionFind = new UnionFind(numberOfNodes);
			int numberOfBits = Integer.parseInt(st.nextToken());

			int node = 1;
			while( (lineRead = br.readLine()) != null) {
				StringBuilder sb = new StringBuilder();
				st = new StringTokenizer(lineRead, " ");
				while (st.hasMoreTokens()) {
					sb.append(st.nextToken());
				}
				String nodeBitsAsString = sb.toString();
				//nodeBitStrings[node-1] = nodeBitsAsString;
				if (nodeBitsToNodeMappping.get(nodeBitsAsString) != null) {
					Integer similarNode = nodeBitsToNodeMappping.get(nodeBitsAsString);
					int[] edgeToAdd = new int[] {similarNode,node};
					distanceToEdgesMapping.get(0).add(edgeToAdd);
				} else {
					nodeBitsToNodeMappping.put(nodeBitsAsString, node);
				}
				
				node ++;
			}
			
			retrieveNodesWithDistanceFromNode(maxDistanceNeeded, nodeBitsToNodeMappping, distanceToEdgesMapping);

		} catch (Exception ex) {
			ex.printStackTrace();
			
		}
		return distanceToEdgesMapping;
	}
	
	 private static void retrieveNodesWithDistanceFromNode(int maxDistanceNeeded,HashMap<String,Integer> nodeBitsToNodeMappping,
			 HashMap<Integer,Set<int[]>> distanceToEdgesMapping) {
		 Set<String> allEdges = new HashSet<>();

		 for (String nodeBitRepresentation: nodeBitsToNodeMappping.keySet()) {
			for (int distance = 1; distance <= maxDistanceNeeded; distance++) {
				Set<String> stringsWithDistance = new HashSet<>();
				createStringWithDistance(nodeBitRepresentation,distance,stringsWithDistance,new HashSet<Integer>()); 
				for (String stringWithDistance:stringsWithDistance) {
					Integer nodeWithDistance = nodeBitsToNodeMappping.get(stringWithDistance);
					if (nodeWithDistance != null) {
						int [] edgeToAdd = new int[] {nodeBitsToNodeMappping.get(nodeBitRepresentation),nodeWithDistance};
						if (distanceToEdgesMapping.get(distance) == null) {
							distanceToEdgesMapping.put(distance,new HashSet<int []>());
						}
						if (!allEdges.contains(edgeToAdd[0] + "_" + edgeToAdd[1]) && !allEdges.contains(edgeToAdd[1] + "_" + edgeToAdd[0])) {
							distanceToEdgesMapping.get(distance).add(edgeToAdd);
							allEdges.add(edgeToAdd[0] + "_" + edgeToAdd[1]);
						} 
					}
				}
			}
		}
	 }
	
	private static String changeAllIndexPositionsInString(String input,Set<Integer> parentIndexes) {
		StringBuilder sb = new StringBuilder(input);
		for (int index:parentIndexes) {
			String ch = sb.substring(index, index+1);
			String newCh = (ch.equals("0"))?"1":"0";
			sb.replace(index, index +1, newCh);
		}
		return sb.toString();
	}
	
	public static void createStringWithDistance(String input,int distance,Set<String> stringsWithDistance,Set<Integer> parentIndexes) {
		int level = distance;
		if (level == 1) {
			for (int i = 0; i < input.length();i++) {
				if (parentIndexes.contains(i)) {
					continue;
				} else {
					parentIndexes.add(i);
					String changedString = changeAllIndexPositionsInString(input, parentIndexes);
					parentIndexes.remove(i);
					stringsWithDistance.add(changedString);
				}
			}
		} else {
			for (int i = 0; i < input.length();i++) {
				if (parentIndexes.contains(i)) {
					continue;
				} else {
					parentIndexes.add(i);
					int newDistance = distance - 1;
					createStringWithDistance(input, newDistance, stringsWithDistance, parentIndexes);
					parentIndexes.remove(i);
				}
				
			}
			
		}
	}
	
	public static void main(String args[]) {
		
//		String input = "111111111111111111111111";
//		Set<Integer> parentIndexes = new HashSet<>();
//		Set<String> stringsWithDistance = new HashSet();
//		createStringWithDistance(input, 2, stringsWithDistance, parentIndexes);
//		System.out.println(input);
//		for (String stringAtDistance:stringsWithDistance) {
//			System.out.println(stringAtDistance);
//		}
//		System.out.println(stringsWithDistance.size());
//		
		long startTime = System.currentTimeMillis();
		String fileName = "C:/Learning/Specialization-Algoriithms/Course3/Week2/clustering_big.txt";
		HashMap<Integer,Set<int[]>> distanceToEdgesMapping = readHammingDistanceFromFile(fileName,3);
		distanceToEdgesMapping.forEach((key,values)-> {
			values.forEach((edge)-> {
				//System.out.println(String.format("(%s,%s,Dist=%d)",edge[0],edge[1],key));	
			});
			System.out.println(String.format("Distance %s has size %s", key,values.size()));
			
		});
		System.out.println("Total time taken:" + (System.currentTimeMillis() - startTime));
				
	}

}
