package com.goodshepherd.datastructures;

public class Stack<E> {
	
	private class Node {
		private E element;
		private Node previous;
		
		Node(E element) {
			this.element = element;
		}
		
		public E getElement() {
			return element;
		}
		
		
		
	}
	
	private Node endPointer;
	private int size;
	
	public void push(E element) {
		Node node = new Node(element);
		Node nodeAtEndPointer = endPointer;
		node.previous = nodeAtEndPointer;
		endPointer = node;
		size ++;
	}
	
	public E pop() {
		Node nodeAtEndPointer = endPointer;
		if (nodeAtEndPointer != null) {
			endPointer = nodeAtEndPointer.previous;
			size--;
			return nodeAtEndPointer.getElement();
		}
		
		return null;

	}
	
	public E peek() {
		Node nodeAtEndPointer = endPointer;
		if (nodeAtEndPointer != null) {
			return nodeAtEndPointer.getElement();
		}
		
		return null;
		
	}
	
	public int getSize() {
		return size;
	}
	
}
