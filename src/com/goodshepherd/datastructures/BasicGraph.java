package com.goodshepherd.datastructures;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import com.goodshepherd.algorithms.graphs.DepthFirstSearch;

public class BasicGraph {

	private Map<Integer,List<Integer>> directedNodeToEdgeMap;
	private int size;
	
	public BasicGraph() {
		this.directedNodeToEdgeMap = new HashMap<>();
	}

	public BasicGraph(Map<Integer,List<Integer>> directedNodeToEdgeMap,int size) {
		this.directedNodeToEdgeMap = directedNodeToEdgeMap;
		this.size = size;
	}
	
	public BasicGraph(Map<Integer,List<Integer>> directedNodeToEdgeMap) {
		this.directedNodeToEdgeMap = directedNodeToEdgeMap;
		setSize();
	}
	
	public void addNodeToEdgeMap(int nodeStart, List<Integer> directedNodeEnds) {
		this.directedNodeToEdgeMap.put(nodeStart, directedNodeEnds);
	}
	
	public BasicGraph generateReverseDirectedGraph() {
		Map<Integer,List<Integer>> reverseDirectionNodeToEdgeMap = new HashMap<>();
		this.directedNodeToEdgeMap.keySet().forEach((node) -> {
			List<Integer> directedNodeEnds = directedNodeToEdgeMap.get(node);
			directedNodeEnds.forEach((endpointNode)-> {
				List<Integer> reverseList = reverseDirectionNodeToEdgeMap.get(endpointNode);
				if (reverseList == null) {
					reverseList = new ArrayList<>();
				}
				reverseList.add(node);
				reverseDirectionNodeToEdgeMap.put(endpointNode, reverseList);
			});
		});
		
		BasicGraph reverseBasicGraph = new BasicGraph(reverseDirectionNodeToEdgeMap);
		
		return reverseBasicGraph;
	}
	
	public Map<Integer,List<Integer>> getDirectedNodeToEdgeMap() {
		return directedNodeToEdgeMap;
	}
	
	private void setSize() {
		Set<Integer> allNodes = new HashSet<>();
		
		this.directedNodeToEdgeMap.keySet().forEach((node) -> {
			List<Integer> directedNodeEnds = directedNodeToEdgeMap.get(node);
			allNodes.add(node);
			allNodes.addAll(directedNodeEnds);
		});

		this.size = allNodes.size();
		
	}
	
	public int getSize() {
		return size;
	}
	
	public List<Integer> performDepthFirstSearchUsingStack(int startVertex) {
		Set<Integer> explored = new HashSet<>();
		List<Integer> visitedOrder = new ArrayList<>();
		Stack<Integer> stack = new Stack<>();
		stack.push(startVertex);
		Map<Integer,List<Integer>> directedNodeToEdgeMap = this.getDirectedNodeToEdgeMap();
		
		while (stack.getSize() > 0) {
			int nodePeeked = stack.peek();
			explored.add(nodePeeked);
			List<Integer> mappedEdges = directedNodeToEdgeMap.get(nodePeeked);
			if (mappedEdges == null) {
				visitedOrder.add(stack.pop());
			} else {
				int count = 0;
				for (Integer node:mappedEdges) {
					if (!explored.contains(node)) {
						stack.push(node);
						count++;
						break;
					}
				}
				
				if (count == 0) {
					visitedOrder.add(stack.pop());
				}
			}
			
		}
		
		return visitedOrder;
	}
	
	
	
	public static List<Integer> deriveFinishTime(BasicGraph graph) {
		Set<Integer> explored = new HashSet<>();
		List<Integer> finishTimeOrder = new ArrayList<>();
		Stack<Integer> stack = new Stack<>();
		Map<Integer,Integer> nodeFinishTimeMap = new TreeMap<>();
		for (int i = graph.getSize();i>0;i--) {
			int startVertex = i;
			if (!explored.contains(startVertex)) {
				stack.push(startVertex);
				Map<Integer,List<Integer>> directedNodeToEdgeMap = graph.getDirectedNodeToEdgeMap();
				
				while (stack.getSize() > 0) {
					int nodePeeked = stack.peek();
					explored.add(nodePeeked);
					List<Integer> mappedEdges = directedNodeToEdgeMap.get(nodePeeked);
					if (mappedEdges == null) {
						finishTimeOrder.add(stack.pop());
					} else {
						int count = 0;
						for (Integer node:mappedEdges) {
							if (!explored.contains(node)) {
								stack.push(node);
								count++;
								break;
							}
						}
						
						if (count == 0) {
							finishTimeOrder.add(stack.pop());
						}
					}
					
				}
				
			}
			
		}
		
		return finishTimeOrder;
	}	
	
	public Map<Integer,List<Integer>> getStronglyConnectedComponents() {
		Map<Integer,List<Integer>> stronglyConnectedNodes = new HashMap<>();
		BasicGraph reverseGraph = this.generateReverseDirectedGraph();
		
		List<Integer> finishTimesOfNodes = deriveFinishTime(reverseGraph);
		Comparator<Integer> comparator = (i1,i2)->{
			return i2 - i1;
		};
		Map<Integer,Integer> finishTimeToNodeMap = new TreeMap<>(comparator);
		int counter = 0;
		for (Integer node:finishTimesOfNodes) {
			counter++;
			finishTimeToNodeMap.put(counter,node);
		}
		
		Set<Integer> explored = new HashSet<>();
		Map<Integer,Integer> sccLeader = new HashMap<>();
		Stack<Integer> stack = new Stack<>();
		
		finishTimeToNodeMap.entrySet().forEach((entry)->{
			//System.out.println("f("+ entry.getValue()+") = " + entry.getKey() );
			int node = entry.getValue();
			
			int startVertex = node;
			if (!explored.contains(startVertex)) {
				int leader = node;
				stack.push(startVertex);
				Map<Integer,List<Integer>> directedNodeToEdgeMap = this.getDirectedNodeToEdgeMap();
				
				while (stack.getSize() > 0) {
					int nodePeeked = stack.peek();
					explored.add(nodePeeked);
					List<Integer> mappedEdges = directedNodeToEdgeMap.get(nodePeeked);
					if (mappedEdges == null) {
						sccLeader.put(stack.pop(), leader);
					} else {
						int count = 0;
						for (Integer nodeOnEdges:mappedEdges) {
							if (!explored.contains(nodeOnEdges)) {
								stack.push(nodeOnEdges);
								count++;
								break;
							}
						}
						
						if (count == 0) {
							sccLeader.put(stack.pop(), leader);
						}
					}
					
				}
				
			}
				
			
			
		});
		
		sccLeader.forEach((key,value)->{
			List<Integer> sccList = stronglyConnectedNodes.get(value);
			if (sccList == null) {
				sccList = new ArrayList<>();
			}
			sccList.add(key);
			stronglyConnectedNodes.put(value, sccList);
		});
		
		
		return stronglyConnectedNodes;
	}
	
	
	public static void main(String args[]) {
		Map<Integer,List<Integer>>  directedNodeToEdgeMap = new HashMap<>();
		
		directedNodeToEdgeMap.put(4, Arrays.asList(2,5));
		directedNodeToEdgeMap.put(2, Arrays.asList(3));
		directedNodeToEdgeMap.put(3, Arrays.asList(4));
		
		directedNodeToEdgeMap.put(5, Arrays.asList(6));
		directedNodeToEdgeMap.put(6, Arrays.asList(1,9));
		directedNodeToEdgeMap.put(1, Arrays.asList(5));
		
		directedNodeToEdgeMap.put(9, Arrays.asList(7));
		directedNodeToEdgeMap.put(7, Arrays.asList(8));
		directedNodeToEdgeMap.put(8, Arrays.asList(9));


		/*
		directedNodeToEdgeMap.put(1, Arrays.asList(7));
		directedNodeToEdgeMap.put(7, Arrays.asList(4,9));
		directedNodeToEdgeMap.put(4, Arrays.asList(1));
		
		directedNodeToEdgeMap.put(9, Arrays.asList(6));
		directedNodeToEdgeMap.put(6, Arrays.asList(3,8));
		directedNodeToEdgeMap.put(3, Arrays.asList(9));
		
		directedNodeToEdgeMap.put(8, Arrays.asList(2));
		directedNodeToEdgeMap.put(2, Arrays.asList(5));
		directedNodeToEdgeMap.put(5, Arrays.asList(8));
		*/
		BasicGraph basicGraph = new BasicGraph(directedNodeToEdgeMap,9);
		
		System.out.println(basicGraph.getStronglyConnectedComponents());
		
		/*
		System.out.println(basicGraph.performDepthFirstSearchUsingStack(9));
		System.out.println(basicGraph.performDepthFirstSearchUsingStack(8));
		System.out.println(basicGraph.performDepthFirstSearchUsingStack(7));
		*/
	}
	
	
}
