package com.goodshepherd.datastructures;

import java.util.ArrayList;
import java.util.List;

public class BinaryNode implements Comparable<BinaryNode> {
	
	private BinaryNode right;
	private BinaryNode left;
	private String id;
	private long nodeWeight;
	private long mergeCount;
	private List<BinaryNode> mergeContributors;
	
	public BinaryNode(String id,long nodeWeight) {
		this.id = id;
		this.nodeWeight = nodeWeight;
	}
	
	public BinaryNode getRight() {
		return right;
	}
	public void setRight(BinaryNode right) {
		this.right = right;
	}
	public BinaryNode getLeft() {
		return left;
	}
	public void setLeft(BinaryNode left) {
		this.left = left;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public long getNodeWeight() {
		return nodeWeight;
	}
	public void setNodeWeight(long nodeWeight) {
		this.nodeWeight = nodeWeight;
	}
	public long getMergeCount() {
		return mergeCount;
	}
	public void setMergeCount(long mergeCount) {
		this.mergeCount = mergeCount;
	}
	
	public void incrementMergeCount() {
		this.mergeCount++;
		
		List<BinaryNode> contributorNodes = this.mergeContributors;
		
		if (contributorNodes != null) {
			contributorNodes.forEach(node->node.incrementMergeCount());
			
		}
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		
		if (!(obj instanceof BinaryNode)) {
			return false;
		}
		
		BinaryNode otherNode = (BinaryNode)obj;
		

		return otherNode.getId() == this.getId();
	}
	
	public BinaryNode mergeTo(BinaryNode binNode) {
		BinaryNode mergedNode = null;
		if (binNode != null) {
			//binNode.mergeCount++;
			//this.mergeCount ++;
			mergedNode = new BinaryNode(this.getId() + "_" + binNode.getId(),
					this.getNodeWeight() + binNode.getNodeWeight());
			if (mergedNode.mergeContributors == null) {
				mergedNode.mergeContributors = new ArrayList<>();
			}
			mergedNode.mergeContributors.add(this);
			mergedNode.mergeContributors.add(binNode);
			mergedNode.mergeContributors.forEach(node -> node.incrementMergeCount());
		}
		return mergedNode;
	}
	
	@Override
	public int compareTo(BinaryNode binNode) {
		return (int) (this.nodeWeight - binNode.nodeWeight);
	}
	
	@Override
	public int hashCode() {
		return this.getId().hashCode();
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("[id:").append(this.getId());
		sb.append(", nodeWeight:").append(nodeWeight);
		sb.append(", mergeCount:").append(mergeCount).append("]");
		return sb.toString();
	}
	


}
