package com.goodshepherd.datastructures;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeSet;
import java.util.stream.Collectors;

public class Graph {
	
	private List<Graph.Edge> [] allNodeEdges;
	private List<Graph.Edge> allEdges = new ArrayList<>();
	
	public Graph(List<Graph.Edge> [] allNodeEdges) {
		this.allNodeEdges = allNodeEdges;
		
		for (int i = 0; i < allNodeEdges.length; i++) {
			List<Graph.Edge> currentNodeEdges = allNodeEdges[i];
			allEdges.addAll(currentNodeEdges);
		}
		
		removeDuplicateEdges();
	}
	
	private void removeDuplicateEdges() {
		final Set <Graph.Edge> removeSet = new HashSet<Graph.Edge>();
		this.allEdges.forEach(edge -> {
			this.allEdges.forEach(edge2 -> {
				if (edge.edgeVertex1 == edge2.edgeVertex2 && edge.edgeVertex2 == edge2.edgeVertex1
						&& edge.edgeVertex1 > edge2.edgeVertex1) {
					removeSet.add(edge2);
				}
			});
		});

		this.allEdges.removeAll(removeSet);
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < allNodeEdges.length; i++ ) {
			sb.append("Node:").append(i+1).append("\n");
			List<Graph.Edge> nodeEdges = allNodeEdges[i];
			if (nodeEdges != null) {
				for (int j = 0 ; j < nodeEdges.size(); j ++) {
					sb.append(nodeEdges.get(j)).append("\n");
				}
			}
		}
		return sb.toString();
	}
	
	public static class Edge {
		private int edgeVertex1;
		private int edgeVertex2;
		
		Edge(int head,int tail){
			this.edgeVertex1 = head;
			this.edgeVertex2 = tail;
		}
		
		public String toString() {
			return String.format("Edge(%s,%s)",edgeVertex1,edgeVertex2);			
		}
		
		public boolean equals(Object o) {
		    if (o == this) {
		      return true;
		    }
		    if (!(o instanceof Edge)) {
		      return false;
		    }
		    Edge edge = (Edge)o;
		    return edge.edgeVertex1 == this.edgeVertex1 && edge.edgeVertex2 == this.edgeVertex2;
		 }
		
	    @Override
	    public int hashCode() {
	        int result = 17;
	        result = 31 * result + this.edgeVertex1;
	        result = 31 * result + this.edgeVertex2;
	        return result;
	    }		
		
	}
	
	public static List<Graph.Edge> [] addMissingNodes(List<Graph.Edge> [] allNodeEdges,TreeSet<Integer> allNodes) {
		List<Graph.Edge> [] completeAllNodeEdges = new List[allNodes.size()];
		System.arraycopy(allNodeEdges, 0, completeAllNodeEdges, 0, allNodeEdges.length);
		for (int i = allNodeEdges.length;i < allNodes.size();i++) {
			completeAllNodeEdges[i] = new ArrayList<Graph.Edge>();
		}
		return completeAllNodeEdges;
	}
	
	public static Graph readFileIntoGraph(String fileName) {
		
		Graph gr = null;
		
		int numberOfLines = 0;
		try (BufferedReader br = new BufferedReader(new FileReader(new File(fileName)))) {
			String lineRead = null;

			while( (lineRead = br.readLine()) != null) {
				numberOfLines++;
			}
		} catch (Exception ex) {
			
		}
		
		try (BufferedReader br = new BufferedReader(new FileReader(new File(fileName)))) {
			String lineRead = null;

			List<Graph.Edge> [] allNodeEdges = new List [numberOfLines];
			TreeSet<Integer> allNodes = new TreeSet<>();
			
			while( (lineRead = br.readLine()) != null) {
				StringTokenizer st = new StringTokenizer(lineRead, "\t");
				int node = Integer.parseInt(st.nextToken());
				List<Edge> nodeEdges = new ArrayList<>();
				while(st.hasMoreTokens()) {
					int vertex1 = node;
					int vertex2 = Integer.parseInt(st.nextToken());
					allNodes.add(vertex1);
					allNodes.add(vertex2);
					Edge edge = new Edge(vertex1,vertex2);
					nodeEdges.add(edge);
				}
				allNodeEdges[node -1] = nodeEdges;
			}
			allNodeEdges = addMissingNodes(allNodeEdges,allNodes);
			gr = new Graph(allNodeEdges);
		} catch (Exception ex) {
			
		}
		return gr;
	}
	
	
	public static int getMinCut(Graph graph,int iterations) {
		int minCut = graph.allNodeEdges.length;
		Random randomI = new Random(42);
		
		long iterStartTime = System.currentTimeMillis();
		
		for (int it = 0; it < iterations;it++) {
		
			List<Graph.Edge> allEdgesList = new ArrayList<>(graph.allEdges.stream().map(ed -> new Graph.Edge(ed.edgeVertex1,ed.edgeVertex2)).collect(Collectors.toList()));
			Set<Integer> nodesSet = new HashSet<>();
			for (int i = 0; i < graph.allNodeEdges.length ;i++) { 
				nodesSet.add(i);
			}
			while(nodesSet.size() > 2) {
				int randomEdgeToMerge = randomI.nextInt(allEdgesList.size());
				Edge edgeToMerge = allEdgesList.get(randomEdgeToMerge);
				final int head = edgeToMerge.edgeVertex1;
				final int tail = edgeToMerge.edgeVertex2;
				allEdgesList.forEach(edge-> {
					if (edge.edgeVertex1 == head) {
						edge.edgeVertex1 = tail;
					} else if (edge.edgeVertex2 == head) {
						edge.edgeVertex2 = tail;
					}
				});
				List<Graph.Edge> selfLoopList = new ArrayList<>();
				allEdgesList.forEach(edge->{
					if (edge.edgeVertex1 == edge.edgeVertex2) {
						selfLoopList.add(edge);
					}
				});
				int selfLoopListSize = selfLoopList.size();
				int allEdgesListSize = allEdgesList.size();
				
				allEdgesList.removeAll(selfLoopList);
				
				//if (allEdgesListSize - allEdgesList.size() != selfLoopListSize) {
				if (false)
					System.out.println(String.format("Size of selfLoopList is %s and size of allEdgesListSize before delete is %s and after delete is %s and self loop set is %s" 
							,selfLoopListSize,allEdgesListSize,allEdgesList.size(),selfLoopList));
				//}
	
				nodesSet.remove(head-1);
			}
			if (minCut > allEdgesList.size() ) {
				minCut = allEdgesList.size();
			}
			if ((it + 1) % 1000 == 0) {
				long newIterStartTime = System.currentTimeMillis();
				System.out.println(String.format("Min cut at %s iteration is %s and took incrementally %s ms",it + 1, minCut,( newIterStartTime - iterStartTime)));
				iterStartTime = newIterStartTime;
			}
		}
		
		return minCut;
		
	}
	
	public static void main(String args[]) {
		long startTime = System.currentTimeMillis();
		Graph graph = readFileIntoGraph("C:/Learning/Specialization-Algoriithms"
				+ "/Course1-Divide and Conquer, Sorting and Searching, and Randomized Algorithms/Week-4"
				//+ "/test.txt");
				+ "/kargerMinCut.txt");
		int n = graph.allNodeEdges.length;
		int numOfIterations = (int) ( n * n * Math.log((double)n));
		System.out.println("Number of iterations is:"+numOfIterations);
		System.out.println("MinCut is " + getMinCut(graph,numOfIterations));
		System.out.println("Total time in secs is " + (System.currentTimeMillis() - startTime)/1000 );
	}

}
