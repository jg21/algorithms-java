package com.goodshepherd.datastructures;

import java.util.HashSet;
import java.util.Set;

public class UnionFind {
	
	private int[] nodeParents;
	private int[] ranks;
	
	public UnionFind(int size) {
		nodeParents = new int[size];
		ranks = new int[size];
		for (int i=0; i < nodeParents.length;i++) {
			nodeParents[i] = i;
			ranks[i] = 0;
		}
	}
	
	public int find(int node) {
		if (node < nodeParents.length) {
			int parent = nodeParents[node];
			if (node == parent) {
				return node;
			} else {
				return find(parent);
			}
		}
		
		return -1;
	}
	
	public void union(int nodeA,int nodeB) {
		int parentA = find(nodeA);
		int parentB = find(nodeB);
		int rankParentA = ranks[parentA];
		int rankParentB = ranks[parentB];
		
		if (rankParentA > rankParentB) {
			nodeParents[parentB] = parentA;
		} else if (rankParentA < rankParentB) {
			nodeParents[parentA] = parentB;
		} else {
			nodeParents[parentA] = parentB;
			ranks[parentB] = ranks[parentB] + 1;
		}
	}
	
	public int getClusterCount() {
		Set<Integer> parents = new HashSet<>();
		for (int i = 0;i< nodeParents.length;i++) {
			parents.add(find(i));
		}
		return parents.size();
	}
	
	public static void main(String args[]) {
		UnionFind unionFind = new UnionFind(5);
		unionFind.union(1, 2);
		unionFind.union(3, 4);
		unionFind.union(0, 1);
		unionFind.union(4, 1);
		System.out.println(unionFind.find(4));
		System.out.println("Rank of 2 is " + unionFind.ranks[2]);
	}

}
