package com.goodshepherd.datastructures;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Heap<T extends Comparable<T>> {
	List<T> heapItems = null;
	Comparator<T> comparator = null;
	
	Comparator<T> MIN_HEAP_COMPARATOR = (parent,child) -> {
			return parent.compareTo(child);
		}; 
	
	Comparator<T> MAX_HEAP_COMPARATOR = (parent,child) -> {
		return child.compareTo(parent);
	}; 		
	
	public Heap( T[] heapItems,boolean isMaxHeap) {
		this.comparator = isMaxHeap?MAX_HEAP_COMPARATOR:MIN_HEAP_COMPARATOR;
		this.heapItems = new ArrayList<>();

		for (T t:heapItems) {
			addElement(t);
		}
	}	
	
	public Heap( T[] heapItems) {
		this(heapItems,false);
	}
	
	public void addElement(T t) {
		this.heapItems.add(t);
		swim(this.heapItems.size() -1);
		
	}
	
	public void swim(int index) {
		int i = index;
		int parentIndex = (i-1)/2;
		
		while (i >0 && comparator.compare(heapItems.get(i), heapItems.get(parentIndex)) < 0) {
			swap(i,parentIndex);
			i = parentIndex;
			parentIndex = (i-1)/2;
		}
	}
	
	public void sink(int index) {
		
		
		while (index < heapItems.size()) {
			int childIndex = 2 * index + 2;
			
			if (childIndex < heapItems.size()) {
				if (comparator.compare(heapItems.get(childIndex),heapItems.get(childIndex-1)) > 0) {
					childIndex = childIndex - 1;
				}
			} else if (childIndex - 1 < heapItems.size()) {
				childIndex = childIndex -1;
			} else {
				break;
			}			

			if (comparator.compare(heapItems.get(index),heapItems.get(childIndex)) > 0) {
				swap(index,childIndex);
			}
			index = childIndex;
		}
	}
	
	public T extractMin() {
		T t = this.heapItems.get(0);
		swap(0,this.heapItems.size()-1);
		this.heapItems.remove(this.heapItems.size() - 1);
		sink(0);
		return t;
	}

	public int size() {
		return this.heapItems.size();
	}

	public void swap(int pos1,int pos2) {
		if (heapItems.size() > Math.max(pos1, pos2) && pos1 != pos2) {
			Collections.swap(heapItems, pos1, pos2);
		}
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for(T t: heapItems) {
			sb.append(t).append(",");
		}
		return sb.toString();
	}
	
	public static void main(String args[]) {
		//Integer[] randomInts = {10,12,15,5,6};
		Integer [] sample = {1};
		int max = (int) Math.pow(10, 3);
		Integer[] randomInts = IntStream.range(0,max).map(i -> max -i).boxed().collect(Collectors.toList()).toArray(sample);
		Heap<Integer> heapOfInts = new Heap<>(randomInts,true);
		//heapOfInts.heapify();
		while(heapOfInts.size() > 0) {
			System.out.println(heapOfInts.extractMin());
		}
	}
}
