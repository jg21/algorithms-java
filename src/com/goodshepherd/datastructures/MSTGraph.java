package com.goodshepherd.datastructures;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeSet;
import java.util.stream.Collectors;

public class MSTGraph {
	// Edge (u,v) for node u is stored as v at [0] and distance at [1]
	private List<Integer[]> [] allNodes;
	private List<Integer[]> allEdges = new ArrayList<>();
	
	public List<Integer[]>[] getAllNodes() {
		return allNodes;
	}

	public List<Integer[]> getAllEdges() {
		return allEdges;
	}
	
	public static MSTGraph initializeEmptyGraphWithNodes(int numberOfNodes) {
		List<Integer[]> [] allNodes = new List[numberOfNodes];
		List<Integer[]> allEdges = new ArrayList<>();
		for (int i =0; i < numberOfNodes;i++) {
			allNodes[i] = new ArrayList<Integer[]>();
		}
		
		return new MSTGraph(allNodes, allEdges);
	}
	
	public void addEdgeDetails(Integer[] edge) {
		int node1 = edge[0];
		int node2 = edge[1];
		int distance = edge[2];
		this.allNodes[node1 -1].add(new Integer[] {node2,distance});
		this.allNodes[node2 -1].add(new Integer[] {node1,distance});
		allEdges.add(edge);
		
	}

	public static MSTGraph createFromFile(String fileName) {
		MSTGraph graph = readFileIntoGraph("C:/Learning/Specialization-Algoriithms/Course3/Week2/clustering1.txt");
		return graph;
	}
	
	public MSTGraph(List<Integer[]> [] allNodes,List<Integer[]> allEdges) {
		this.allNodes = allNodes;
			
		this.allEdges = allEdges;
		allEdges.sort( (a, b) -> {
			return a[2].compareTo(b[2]);
		} );
	}
	
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < allNodes.length; i++ ) {
			sb.append("***************************\n");
			sb.append("Node:").append(i+1).append("\n");
			List<Integer[]> nodes = allNodes[i];
			if (nodes != null) {
				for (int j = 0 ; j < nodes.size(); j ++) {
					sb.append(String.format("Edge:(%s,%s),Distance:%s", i+1,nodes.get(j)[0],nodes.get(j)[1])).append("\n");
				}
			}
			sb.append("***************************\n");
		}
		
		for (Integer[] edgeDetails:allEdges) {
			sb.append(String.format("Edge:(%s,%s),Distance:%s", edgeDetails[0] , edgeDetails[1] , edgeDetails[2]));
			sb.append("\n");
		}
		return sb.toString();
	}
	
	
	public static MSTGraph readFileIntoGraph(String fileName) {
		
		MSTGraph gr = null;
		
		int numberOfLines = 0;

		
		try (BufferedReader br = new BufferedReader(new FileReader(new File(fileName)))) {
			String lineRead = null;
			numberOfLines = Integer.parseInt(br.readLine());
			List<Integer[]> [] allNodes = new List[numberOfLines];
			List<Integer[]> allEdges = new ArrayList<>();
			for (int i =0; i < numberOfLines;i++) {
				allNodes[i] = new ArrayList<Integer[]>();
			}
			
			while( (lineRead = br.readLine()) != null) {
				StringTokenizer st = new StringTokenizer(lineRead, " ");
				int node1 = Integer.parseInt(st.nextToken());
				int node2 = Integer.parseInt(st.nextToken());
				int distance = Integer.parseInt(st.nextToken());
				allNodes[node1 - 1].add(new Integer[] {node2,distance});
				allNodes[node2 - 1].add(new Integer[] {node1,distance});
				allEdges.add(new Integer[] {node1,node2,distance});
			}
			gr = new MSTGraph(allNodes,allEdges);
		} catch (Exception ex) {
			
		}
		return gr;
	}
	
	
	public static void main(String args[]) {
		long startTime = System.currentTimeMillis();
		MSTGraph graph = readFileIntoGraph("C:/Learning/Specialization-Algoriithms/Course3/Week2/clustering1.txt");
		System.out.println(graph);
		System.out.println("Total time in secs is " + (System.currentTimeMillis() - startTime)/1000 );
	}

}
