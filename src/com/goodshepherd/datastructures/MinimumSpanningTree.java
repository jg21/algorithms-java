package com.goodshepherd.datastructures;

import java.util.List;

public class MinimumSpanningTree {
	private int cost;
	private List<Integer[]> edges;
	
	
	public MinimumSpanningTree(int cost,List<Integer[]> edges) {
		this.cost = cost;
		this.edges = edges;
	}
	
	public int getCost() {
		return cost;
	}

	public List<Integer[]> getEdges() {
		return edges;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Cost:").append(this.cost).append("\n");
		this.edges.forEach(edge -> sb.append(String.format("Node1:%s,Node2:%s,Distance:%s", edge[0] , edge[1], edge[2])));
		return sb.toString();
	}


}
