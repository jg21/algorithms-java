package com.goodshepherd.algorithms;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.goodshepherd.datastructures.BinaryNode;

public class HuffmanEncoding {
	
	public static final String FILE_NAME = "C:/Learning/Specialization-Algoriithms/Course3/Week3/huffman.txt";

	public static void main(String[] args) {
		List<BinaryNode> allNodes = readSymbolsFromFile(FILE_NAME);
		BinaryNode tree = huffmann(allNodes);
		List<Long> nodeTreeLengths = new ArrayList();
		
		allNodes.forEach(node->nodeTreeLengths.add( node.getMergeCount()) );
		
		System.out.println("Minimum length:" + nodeTreeLengths.stream().min((a,b) -> {
			return (int)(a - b);
		}).get());

		System.out.println("Maximum length:" + nodeTreeLengths.stream().max((a,b) -> {
			return (int)(a - b);
		}).get());

		
	}
	
	public static List<BinaryNode>readSymbolsFromFile(String fileName) {
		List<BinaryNode> allNodes = new ArrayList<>();
		
		try(BufferedReader br = new BufferedReader(new FileReader(fileName))) {
			String lineRead = br.readLine();
			int numberOfSymbols = Integer.parseInt(lineRead);
			int id = 1;
			while ( (lineRead = br.readLine()) != null ) {
				BinaryNode binNode = new BinaryNode(String.valueOf(id), Integer.parseInt(lineRead));
				allNodes.add(binNode);
				id ++;
			}
			//Collections.sort(allNodes);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return allNodes;
	}
	
	public static BinaryNode huffmann(List<BinaryNode> nodes) {
		BinaryNode binaryNode = null;
		Collections.sort(nodes);
		BinaryNode node1 = nodes.get(0);
		BinaryNode node2 = nodes.get(1);
		if (nodes.size() == 2) {
			binaryNode = new BinaryNode(node1.getId() + "_"+node2.getId(),
					node1.getNodeWeight() + node2.getNodeWeight());
			node1.incrementMergeCount();
			node2.incrementMergeCount();
			binaryNode.setLeft(node1);
			binaryNode.setRight(node2);
			return binaryNode;
		}
		
		List<BinaryNode> mergedNodeList = new ArrayList<>(nodes);
		
		BinaryNode newNode = node1.mergeTo(node2);
		mergedNodeList.remove(0);
		mergedNodeList.remove(0);
		mergedNodeList.add(newNode);
		BinaryNode compressedTree = huffmann(mergedNodeList);
		
		newNode.setLeft(node1);
		newNode.setRight(node2);
		
		return newNode;
		
	}

}
