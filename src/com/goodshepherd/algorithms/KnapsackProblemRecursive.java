package com.goodshepherd.algorithms;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;

public class KnapsackProblemRecursive {
	
	int capacity;
	int [] elementWeights;
	int [] elementValues;
	int numOfElements;
	public static final String FILE_NAME = "C:/Learning/Specialization-Algoriithms/Course3/Week4/knapsack_big.txt";
	private Map<String,Long> keyToValueMappings = new HashMap<>();
	
	public KnapsackProblemRecursive(int capacity, int [] elementWeights, int [] elementValues) {
		this.capacity = capacity;
		this.elementWeights = elementWeights;
		this.elementValues = elementValues;
		numOfElements = elementValues.length;
	}
	
	public long optimalKnapsackSolution(int numElements,int capacity) {
		if (numElements == 0) {
			return 0;
		}
		
		if (elementWeights[numElements] > capacity) {
			String key = (numElements -1) + "_" + capacity;
			Long value = keyToValueMappings.get(key);
			if (value == null) {
				value= optimalKnapsackSolution(numElements -1, capacity);
				keyToValueMappings.put(key, value);
			}
			return value;
		} else {
			String key = (numElements-1) + "_" + (capacity - elementWeights[numElements]);
			Long value = keyToValueMappings.get(key);
			if (value == null) {
				value = optimalKnapsackSolution(numElements-1, capacity - elementWeights[numElements]);
				keyToValueMappings.put(key, value);
			}
			long includingCurrent = value + elementValues[numElements];
			key = (numElements -1) + "_" + capacity;
			value = keyToValueMappings.get(key);
			if (value == null) {
				value = optimalKnapsackSolution(numElements -1, capacity);
				keyToValueMappings.put(key, value);
			}
			long excludingCurrent = value;
			return Math.max(includingCurrent,excludingCurrent);
		}
	}
	
	public static void main(String[] args) {
		try (BufferedReader br = new BufferedReader(new FileReader(FILE_NAME))) {
			String lineRead = br.readLine();
			String[] firstLine = lineRead.split(" ");
			int capacity = Integer.parseInt(firstLine[0]);
			int numOfElements = Integer.parseInt(firstLine[1]);
			int [] elementWeights = new int[numOfElements+1];
			int [] elementValues = new int[numOfElements+1];
			elementValues[0] = 0;
			elementWeights[0] = 0;
			int i = 1;
			while ( (lineRead = br.readLine()) != null ) {
				String [] lineContents = lineRead.split(" ");
				elementValues[i] = Integer.parseInt(lineContents[0]);
				elementWeights[i] = Integer.parseInt(lineContents[1]);
				i++;
			}
			
			KnapsackProblemRecursive knapsackProblem = new KnapsackProblemRecursive(capacity, elementWeights, elementValues);
			System.out.println(knapsackProblem.optimalKnapsackSolution(numOfElements,capacity));
			

		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

}
