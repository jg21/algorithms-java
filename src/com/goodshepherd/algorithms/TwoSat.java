package com.goodshepherd.algorithms;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.goodshepherd.datastructures.BasicGraph;

public class TwoSat {

	private int[] nodes;
	
	public TwoSat(int size) {
		this.nodes = new int[2*size];
		/**
		 * Put negative nodes in index 0 to size -1 and positive nodes in index size  to 2*size -1 
		 */
	}
	
	public static void main(String args[]) {
		String directory = "C:\\Learning\\Specialization-Algorithms\\Shortest Paths Revisited, NP-Complete Problems and What To Do About Them\\Week4\\";
		String[] fileList = new String[] {"2sat1.txt","2sat2.txt","2sat3.txt","2sat4.txt","2sat5.txt","2sat6.txt"}; 
		StringBuilder sb = new StringBuilder();
		for (String file:fileList) {
			String fileName = directory + file;
			boolean unSatisfied = areTwoSatConstraintsUnsatisfied(fileName);
			System.out.println(String.format("fileName:%s,unSatisfied:%s",file,unSatisfied));
			sb.append(unSatisfied?"0":"1");
		}
		
		System.out.println(sb.toString());
		
	}
	
	public static boolean areTwoSatConstraintsUnsatisfied(String fileName) {
		
		BasicGraph basicGraph = readFromFile(fileName);
		System.out.println("Size:"+basicGraph.getSize());
		int size = basicGraph.getSize();
		Collection<List<Integer>> collectionOfLists = basicGraph.getStronglyConnectedComponents().values();
		boolean unSatisfied = false;
		for (List<Integer> nodeList:collectionOfLists) {
			for (int i = 0; i < nodeList.size();i++) {
				for (int j = i +1; j < nodeList.size();j++) {
					if (nodeList.get(i) - nodeList.get(j) == size || nodeList.get(i) - nodeList.get(j) == -size) {
						System.out.println(String.format("i=%s,j=%s", i,j));
						unSatisfied = true;
						break;
					}
				}
			}
			
		}
		
		return unSatisfied;
		
		
	}
	
	/*
	 *  x1 or x2  implies x1=0 -> x2 = 1 and x2 = 0 -> x1 =1
	 * -x1 or x2  implies x1=1 -> x2 = 1 and x2 = 0 -> x1 =0
	 *  x1 or -x2 implies x1=0 -> x2 = 0 and x2 = 1 -> x1 =1
	 * -x1 or -x2 implies x1=1 -> x2 = 0 and x2 = 1 -> x1 =0
	 */
	
	public int getNodeIndex(int node) {
		if (node > 0) {
			return (this.nodes.length/2 -1) + node;
		} else {
			return -(node+1);
		}
	}
	
	public static BasicGraph readFromFile(String fileName) {
		Map<Integer,List<Integer>>  directedNodeToEdgeMap = new HashMap<>();
		BasicGraph basicGraph = null;
		
		try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
			String lineRead = br.readLine();
			int count = Integer.parseInt(lineRead);
			TwoSat twoSat = new TwoSat(count);
			
			while ( (lineRead = br.readLine()) != null) {
				String[] lineParts = lineRead.split(" ");
				int node1 = Integer.parseInt(lineParts[0]);
				int node2 = Integer.parseInt(lineParts[1]);
				
				int node1Index = twoSat.getNodeIndex(node1);
				int node2Index = twoSat.getNodeIndex(node2);
				
				if (node1 > 0 && node2 > 0) {
					int negativeNode1Index = twoSat.getNodeIndex(-node1);
					int negativeNode2Index = twoSat.getNodeIndex(-node2);
					List<Integer> nodeToEdgeList = directedNodeToEdgeMap.get(negativeNode1Index);
					if (nodeToEdgeList == null) {
						nodeToEdgeList = new ArrayList<>();
					}
					nodeToEdgeList.add(twoSat.getNodeIndex(node2));
					directedNodeToEdgeMap.put(negativeNode1Index, nodeToEdgeList);
					
					nodeToEdgeList = directedNodeToEdgeMap.get(negativeNode2Index);
					if (nodeToEdgeList == null) {
						nodeToEdgeList = new ArrayList<>();
					}
					nodeToEdgeList.add(twoSat.getNodeIndex(node1));
					directedNodeToEdgeMap.put(negativeNode2Index, nodeToEdgeList);
				} else if (node1 < 0 && node2 > 0) {
					int postiveNode1Index = twoSat.getNodeIndex(-node1);
					int positiveNode2Index = twoSat.getNodeIndex(node2);
					int negativeNode2Index = twoSat.getNodeIndex(-node2);
					int negativeNode1Index = twoSat.getNodeIndex(node1);
					List<Integer> nodeToEdgeList = directedNodeToEdgeMap.get(postiveNode1Index);
					if (nodeToEdgeList == null) {
						nodeToEdgeList = new ArrayList<>();
					}
					nodeToEdgeList.add(positiveNode2Index);
					directedNodeToEdgeMap.put(postiveNode1Index, nodeToEdgeList);
					
					nodeToEdgeList = directedNodeToEdgeMap.get(negativeNode2Index);
					if (nodeToEdgeList == null) {
						nodeToEdgeList = new ArrayList<>();
					}
					nodeToEdgeList.add(negativeNode1Index);
					directedNodeToEdgeMap.put(negativeNode2Index, nodeToEdgeList);
					
				} else if (node1 > 0 && node2 < 0) {//x1 or -x2 implies x1=0 -> x2 = 0 and x2 = 1 -> x1 =1
					int postiveNode1Index = twoSat.getNodeIndex(node1);
					int positiveNode2Index = twoSat.getNodeIndex(-node2);
					int negativeNode1Index = twoSat.getNodeIndex(-node1);
					int negativeNode2Index = twoSat.getNodeIndex(node2);
					List<Integer> nodeToEdgeList = directedNodeToEdgeMap.get(negativeNode1Index);
					if (nodeToEdgeList == null) {
						nodeToEdgeList = new ArrayList<>();
					}
					nodeToEdgeList.add(negativeNode2Index);
					directedNodeToEdgeMap.put(negativeNode1Index, nodeToEdgeList);
					
					nodeToEdgeList = directedNodeToEdgeMap.get(positiveNode2Index);
					if (nodeToEdgeList == null) {
						nodeToEdgeList = new ArrayList<>();
					}
					nodeToEdgeList.add(postiveNode1Index);
					directedNodeToEdgeMap.put(positiveNode2Index, nodeToEdgeList);
					
				} else {//-x1 or -x2 implies x1=1 -> x2 = 0 and x2 = 1 -> x1 =0
					int postiveNode1Index = twoSat.getNodeIndex(-node1);
					int positiveNode2Index = twoSat.getNodeIndex(-node2);
					int negativeNode1Index = twoSat.getNodeIndex(node1);
					int negativeNode2Index = twoSat.getNodeIndex(node2);
					List<Integer> nodeToEdgeList = directedNodeToEdgeMap.get(postiveNode1Index);
					if (nodeToEdgeList == null) {
						nodeToEdgeList = new ArrayList<>();
					}
					nodeToEdgeList.add(negativeNode2Index);
					directedNodeToEdgeMap.put(postiveNode1Index, nodeToEdgeList);
					
					nodeToEdgeList = directedNodeToEdgeMap.get(positiveNode2Index);
					if (nodeToEdgeList == null) {
						nodeToEdgeList = new ArrayList<>();
					}
					nodeToEdgeList.add(negativeNode1Index);
					directedNodeToEdgeMap.put(positiveNode2Index, nodeToEdgeList);
				}
			}
			
			basicGraph = new BasicGraph(directedNodeToEdgeMap,count);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return basicGraph;
	}
}
