package com.goodshepherd.algorithms.graphs;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FloydWarshallAllPairsShortestPath {
	
	private int [] [] [] shortestPaths;
	private int [] [] predecessorPointers;
	private List<Integer> shortestPathLengths;
	private Map<String, Integer> edgeDistanceMap = new HashMap<>();
	public static final String FILENAME_1 = "C:/Learning/Specialization-Algoriithms/Course4/Week1/g1.txt";
	public static final String FILENAME_2 = "C:/Learning/Specialization-Algoriithms/Course4/Week1/g2.txt";
	public static final String FILENAME_3 = "C:/Learning/Specialization-Algoriithms/Course4/Week1/g3.txt";
	
	public FloydWarshallAllPairsShortestPath(String fileName) throws Exception {
		int numOfNodes = 0;
		int maxDistance = 0;
		int INFINITY_PROXY = 0;
		try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
			String lineRead = br.readLine();
			String[] firstLineContents = lineRead.split(" ");
			numOfNodes = Integer.parseInt(firstLineContents[0]);
			int numOfEdges = Integer.parseInt(firstLineContents[1]);
			List<Edge> edges = new ArrayList<>(); 
			while ( (lineRead = br.readLine()) != null ) {
				String[] edgeDetails = lineRead.split(" ");
				int distance = Integer.parseInt(edgeDetails[2]);
				Edge edge = new Edge(
						Integer.parseInt(edgeDetails[0]),
						Integer.parseInt(edgeDetails[1]),
						distance
						);
				edges.add(edge);
				if (distance > maxDistance) {
					maxDistance = distance;
				}
				edgeDistanceMap.put(edge.getKey(), edge.getLength());
			}
		} catch (Exception ex) {
			
		}	
		
		INFINITY_PROXY = (maxDistance+100) * numOfNodes;
		
		shortestPaths = new int [numOfNodes+1] [numOfNodes+1] [2];
		predecessorPointers = new int [numOfNodes+1] [numOfNodes+1];
		for ( int i = 1; i <=numOfNodes;i++) {
			for (int j = 1;j <= numOfNodes;j++) {
				if (i == j) {
					shortestPaths[i][j][0] = 0;
				} else if (edgeDistanceMap.containsKey(Edge.formKey(i, j))) {
					shortestPaths[i][j][0] = edgeDistanceMap.get(Edge.formKey(i, j));
				} else {
					shortestPaths[i][j][0] = INFINITY_PROXY;
				}
			}
		}
		
		shortestPathLengths = new ArrayList<>();
		
		for (int k =1; k <= numOfNodes;k++) {
			for (int i = 1; i <= numOfNodes; i++) {
				for (int j = 1; j <= numOfNodes;j++) {
					shortestPaths[i][j][1] = Math.min(shortestPaths[i][j][0], shortestPaths[i][k][0] + shortestPaths[k][j][0]);
					if (shortestPaths[i][k][0] + shortestPaths[k][j][0] < shortestPaths[i][j][0]) {
						predecessorPointers[i][j] = k;
					}
					
					if ( i == j && k == numOfNodes) {
						if (shortestPaths[i][j][1] < 0) {
							throw new Exception(String.format("Graph from file %s contains a negative cost cycle. So exiting",fileName));
						}
					}
					
					if (k == numOfNodes) {
						shortestPathLengths.add(shortestPaths[i][j][1]);
					}
				}
			}
			
			if (k < numOfNodes) {
				for (int i = 1; i <= numOfNodes; i++) {
					for (int j = 1; j <= numOfNodes;j++) {
						shortestPaths[i][j][0] = shortestPaths[i][j][1];
					}
				}				
			} 
		}
		
		
	}
	

	public static void main(String[] args) {
		String fileName = FILENAME_1;
		try {
			FloydWarshallAllPairsShortestPath floydWarshallAllPairsShortestPath = new FloydWarshallAllPairsShortestPath(fileName);
			floydWarshallAllPairsShortestPath.shortestPathLengths.sort((o1,o2)-> {return 01 - 02;});
			System.out.println(String.format("Shortest path length for graph in file %s is %s", fileName,floydWarshallAllPairsShortestPath.shortestPathLengths.get(0)));
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		
		fileName = FILENAME_2;
		try {
			FloydWarshallAllPairsShortestPath floydWarshallAllPairsShortestPath = new FloydWarshallAllPairsShortestPath(fileName);
			floydWarshallAllPairsShortestPath.shortestPathLengths.sort((o1,o2)-> {return 01 - 02;});
			System.out.println(String.format("Shortest path length for graph in file %s is %s", fileName,floydWarshallAllPairsShortestPath.shortestPathLengths.get(0)));
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		
		fileName = FILENAME_3;
		try {
			FloydWarshallAllPairsShortestPath floydWarshallAllPairsShortestPath = new FloydWarshallAllPairsShortestPath(fileName);
			floydWarshallAllPairsShortestPath.shortestPathLengths.sort(null);
			System.out.println(String.format("Shortest path length for graph in file %s is %s", fileName,floydWarshallAllPairsShortestPath.shortestPathLengths.get(0)));
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}

	}
	
	public static class Edge {
		private int head, tail, length;
		Edge(int head, int tail, int length) {
			this.head = head;
			this.tail = tail;
			this.length = length;
		}
		
		public static String formKey(int head, int tail) {
			return head + "->" + tail;
		}
		
		public String getKey() {
			return formKey(head, tail);
		}
		
		public int getLength() {
			return this.length;
		}
		
		@Override
		public boolean equals(Object obj) {
			if (obj == null || ! (obj instanceof Edge) ) {
				return false;
			}
			Edge castEdge = (Edge)obj;
			return this.head == castEdge.head && this.tail == castEdge.tail && this.length == castEdge.length;
		}
	}

}
