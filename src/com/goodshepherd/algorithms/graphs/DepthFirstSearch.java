package com.goodshepherd.algorithms.graphs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.goodshepherd.datastructures.BasicGraph;
import com.goodshepherd.datastructures.Stack;

public class DepthFirstSearch {
	
	private Set<Integer> explored;
	private List<Integer> visitedOrder;
	private Map<Integer, Integer> finishTimeMapping;
	public DepthFirstSearch(BasicGraph basicGraph) {
		this.explored = new HashSet<>();
		visitedOrder = new ArrayList<>();
		finishTimeMapping = new HashMap<>(); 
	}
	
	public void performDepthFirstSearch(BasicGraph basicGraph,int startVertex) {
		explored.add(startVertex);
		Map<Integer,List<Integer>> directedNodeToEdgeMap = basicGraph.getDirectedNodeToEdgeMap();
		List<Integer> mappedEdges = directedNodeToEdgeMap.get(startVertex);
		if (mappedEdges != null) {
			mappedEdges.forEach((node) -> {
				if (!explored.contains(node)) {
					performDepthFirstSearch(basicGraph, node);
				}
			});
		}
		visitedOrder.add(startVertex);
	}
	
	public void performDepthFirstSearchUsingStack(BasicGraph basicGraph,int startVertex) {
		Stack<Integer> stack = new Stack<>();
		stack.push(startVertex);
		Map<Integer,List<Integer>> directedNodeToEdgeMap = basicGraph.getDirectedNodeToEdgeMap();
		int graphSize = basicGraph.getSize();
		int finishTime = 0;
		
		while (stack.getSize() > 0) {
			int nodePeeked = stack.peek();
			explored.add(nodePeeked);
			List<Integer> mappedEdges = directedNodeToEdgeMap.get(nodePeeked);
			if (mappedEdges == null) {
				visitedOrder.add(stack.pop());
			} else {
				int count = 0;
				for (Integer node:mappedEdges) {
					if (!explored.contains(node)) {
						stack.push(node);
						count++;
						break;
					}
				}
				
				if (count == 0) {
					visitedOrder.add(stack.pop());
				}
			}
			
		}
		
	}
	
	public static void main(String args[]) {
		Map<Integer,List<Integer>>  directedNodeToEdgeMap = new HashMap<>();
		
		directedNodeToEdgeMap.put(1, Arrays.asList(2,3));
		directedNodeToEdgeMap.put(2, Arrays.asList(4));
		directedNodeToEdgeMap.put(4, Arrays.asList(6));
		directedNodeToEdgeMap.put(3, Arrays.asList(4,5));
		directedNodeToEdgeMap.put(5, Arrays.asList(6));
		
		BasicGraph basicGraph = new BasicGraph(directedNodeToEdgeMap);
		/*
		DepthFirstSearch depthFirstSearch = new DepthFirstSearch(basicGraph);
		depthFirstSearch.performDepthFirstSearch(basicGraph,1);
		System.out.println(depthFirstSearch.visitedOrder);
		*/
		
		DepthFirstSearch depthFirstSearchUsingStack = new DepthFirstSearch(basicGraph);
		depthFirstSearchUsingStack.performDepthFirstSearchUsingStack(basicGraph,1);
		System.out.println(depthFirstSearchUsingStack.visitedOrder);
	}
	
}
