package com.goodshepherd.algorithms.graphs;

import java.io.BufferedReader;
import java.io.FileReader;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.stream.Collectors;

public class TravellingSalesmanHeuristic {
	
	public static final String FILE_NAME = "C:\\Projects\\Eclipse_Projects\\algorithms-java\\src\\resources\\nn.txt";

	public static class NodeDistance {
		private int toNodeAddress;
		private double distance;
		
		public NodeDistance(int toNodeAddress, double distance) {
			this.toNodeAddress = toNodeAddress;
			this.distance = distance;
		}
		public int getToNodeAddress() {
			return toNodeAddress;
		}
		public void setToNodeAddress(int toNodeAddress) {
			this.toNodeAddress = toNodeAddress;
		}
		public double getDistance() {
			return distance;
		}
		public void setDistance(double distance) {
			this.distance = distance;
		}
		@Override
		public String toString() {
			return "NodeDistance [toNodeAddress=" + toNodeAddress + ", distance=" + distance + "]";
		}
		
	}
	
	public static void allInOneShot() {
		Set<Integer> nodeVisited = new HashSet<>();
		try (BufferedReader br = new BufferedReader(new FileReader(FILE_NAME))) {
			String lineRead = br.readLine();
			int numOfNodes = Integer.parseInt(lineRead);
			
			double[] xCods = new double[numOfNodes];
			double[] yCods = new double[numOfNodes];
			int idx = 0;
			while ( (lineRead = br.readLine()) != null) {
				String [] coordinates = lineRead.split(" ");
				xCods[idx] = Double.parseDouble(coordinates[1]);
				yCods[idx] = Double.parseDouble(coordinates[2]);
				idx++;
			}
			
			double totalDistance = 0;
			
			int startNode = 0;
			while (nodeVisited.size() < (numOfNodes-1) ) { 
				nodeVisited.add(startNode);
				Set<NodeDistance> distancesFromStartNode = computeEdgeLengths(xCods, yCods,startNode);
				NodeDistance shortestNotVisitedNodeDistance = null;
				for (NodeDistance distanceFromStartNode:distancesFromStartNode) {
					if (!nodeVisited.contains(distanceFromStartNode.toNodeAddress)) {
						shortestNotVisitedNodeDistance = distanceFromStartNode;
						break;
					}
				}
				NodeDistance nextNode = shortestNotVisitedNodeDistance;
				startNode = nextNode.getToNodeAddress();
				totalDistance += nextNode.getDistance();
				if (nodeVisited.size()%1000==0) {
					System.out.println(nodeVisited.size());
				}
			}
			
			System.out.println("Penultimate node before 0 is" + startNode);
			
			totalDistance += computeDistance(xCods, yCods, startNode, 0);
			
			System.out.println("Total distance is:"+ (int)totalDistance);
			
			
		} catch (Exception ex) {
			ex.printStackTrace();
		}


	}
	
	public static Set<NodeDistance> computeEdgeLengths(double [] xCods, double[] yCods,int startNode) {
		double[] edgeLengthsFromStartNode = new double[xCods.length];
		
		Comparator<NodeDistance> comparator = (n1,n2) -> {
			if (n1.distance > n2.distance) {
				return 1;
			} else if (n1.distance < n2.distance) {
				return -1;
			} else {
				if (n1.toNodeAddress > n2.toNodeAddress) {
					return 1;
				} else if (n2.toNodeAddress > n1.toNodeAddress) {
					return -1;
				}
				return 0;
			}
		};
		
		Set<NodeDistance> distancesFromStartNode = new TreeSet<>(comparator);
		
		for (int l = 0; l < xCods.length;l++) {
			if (l!= startNode) {
				double distance = computeDistance(xCods, yCods, startNode, l);
				distancesFromStartNode.add(new NodeDistance(l,distance));
			}
		}
		
		return distancesFromStartNode;
		
	}
	
	public static double computeDistance(double [] xCods, double[] yCods, int startNodeIndex, int endNodeIndex) {
		return Math.pow(
				Math.pow((xCods[startNodeIndex] - xCods[endNodeIndex]),2) + Math.pow((yCods[startNodeIndex] - yCods[endNodeIndex]),2),
				0.5);
	}
	
	public static void main(String[] args) {
		Instant start = Instant.now();
		//trial();
		allInOneShot();
		//iterative();
		System.out.println("Compeltion Time in millis is " + Duration.between(start, Instant.now()).toMillis());
	}
	
	
	
	

}
