package com.goodshepherd.algorithms.graphs;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import com.goodshepherd.datastructures.MSTGraph;

public class BreadthFirstSearch {
	
	public static final String FILENAME = "C:/Learning/Specialization-Algoriithms/Course3/Week2/clustering1.txt";
	
	public static void main(String args[]) {
		MSTGraph graph = MSTGraph.createFromFile(FILENAME);
		System.out.println( doesPathExist(graph, 1, 300) );
	}
	
	public static boolean doesPathExist(MSTGraph graph, int startVertex,int targetVertex) {

		List<Integer> nodesReached = new ArrayList<>();
		if (graph.getAllNodes().length <= 0) {
			return false;
		}
		boolean [] visitedNodes = new boolean[graph.getAllNodes().length];
		//initialize visitedNodes to startVertex
		LinkedList<Integer> queue = new LinkedList<>();
		queue.add(startVertex);
		visitedNodes[startVertex -1] = true;
		
		while (!queue.isEmpty()) {
			int vertex = queue.poll();
			nodesReached.add(vertex);
			List<Integer[]> nodeDetails = graph.getAllNodes()[vertex - 1];
			for (Integer[] nodeDetail:nodeDetails) {
				int node = nodeDetail[0];
				int distance = nodeDetail[1];
				if (!visitedNodes[node - 1]) {
					queue.add(node);
					visitedNodes[node - 1] = true;
				}
			}
		}
		
//		StringBuilder sb = new StringBuilder();
//		nodesReached.forEach(n -> sb.append(String.format("->%s",n)) );
//		System.out.println(sb);
		
		return visitedNodes[targetVertex -1];

	}

}
