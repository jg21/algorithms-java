package com.goodshepherd.algorithms.graphs;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import com.goodshepherd.datastructures.MSTGraph;
import com.goodshepherd.datastructures.MinimumSpanningTree;
import com.goodshepherd.datastructures.UnionFind;
import com.goodshepherd.utilties.HammingDistanceUtility;

public class KruskalsMST {

	public static final String FILENAME = "C:/Learning/Specialization-Algoriithms/Course3/Week2/clustering1.txt";
	
	public static MinimumSpanningTree minimumSpanningTree(String fileName) {
		MSTGraph graph = MSTGraph.createFromFile(fileName);
		MSTGraph mstGraph = MSTGraph.initializeEmptyGraphWithNodes(graph.getAllNodes().length);
		List<Integer[]> allEdges = graph.getAllEdges();
		int cost = 0;
		for (Integer[] edgeDetail:allEdges) {
			int node1=edgeDetail[0];
			int node2=edgeDetail[1];
			int distance = edgeDetail[2];
			if (! BreadthFirstSearch.doesPathExist(mstGraph, node1, node2)) {
				mstGraph.addEdgeDetails(edgeDetail);
				cost += distance;
			}
		}
		MinimumSpanningTree mst = new MinimumSpanningTree(cost, mstGraph.getAllEdges());
		return mst;
	}
	
	public static void clusterEdge(Integer[] edgeDetail,HashMap<Integer, Integer> clusterRank,List<Integer> clusterAddresses) {
		int node1 = edgeDetail[0];
		int node2 = edgeDetail[1];
		
		int node1Cluster = clusterAddresses.get(node1 -1);
		int node2Cluster = clusterAddresses.get(node2 -1);
		
		int combinedRank = clusterRank.get(node1Cluster) + clusterRank.get(node2Cluster);
		if (clusterRank.get(node1Cluster) > clusterRank.get(node2Cluster)) {
			clusterRank.put(node1Cluster, combinedRank);
			clusterRank.put(node2Cluster, 0);
			for (int i = 0; i < clusterAddresses.size();i++) {
				if (clusterAddresses.get(i) == node2Cluster) {
					clusterAddresses.set(i, node1Cluster);
				}
			}
		} else {
			clusterRank.put(node2Cluster, combinedRank);
			clusterRank.put(node1Cluster, 0);
			for (int i = 0; i < clusterAddresses.size();i++) {
				if (clusterAddresses.get(i) == node1Cluster) {
					clusterAddresses.set(i, node2Cluster);
				}
			}
		}
	}
	
	public static int clustering(String fileName,int maxClusters) {
		int maxSpacing = 0;
		MSTGraph graph = MSTGraph.createFromFile(fileName);
		MSTGraph mstGraph = MSTGraph.initializeEmptyGraphWithNodes(graph.getAllNodes().length);
		HashMap<Integer, Integer> clusterRank = new HashMap<>();
		List<Integer> clusterAddresses = new ArrayList<>();
		for (int i = 0; i < graph.getAllNodes().length;i++) {
			clusterRank.put(i+1,1);
			clusterAddresses.add(i+1);
		}
		List<Integer[]> allEdges = graph.getAllEdges();
		int cost = 0;
		for (Integer[] edgeDetail:allEdges) {
			int node1=edgeDetail[0];
			int node2=edgeDetail[1];
			int distance = edgeDetail[2];
			if (! BreadthFirstSearch.doesPathExist(mstGraph, node1, node2)) {
				mstGraph.addEdgeDetails(edgeDetail);
				clusterEdge(edgeDetail,clusterRank,clusterAddresses);
				cost += distance;
				int distinctClusterAddresses = (int)clusterAddresses.stream().distinct().count();
				System.out.println(distinctClusterAddresses);
				if ( distinctClusterAddresses < maxClusters) {
					maxSpacing = distance;
					break;
				}
			} else {
				//System.out.println(String.format("Not addiing edge:%s->%s to avoid cycle",node1,node2));
			}
		}
		MinimumSpanningTree mst = new MinimumSpanningTree(cost, mstGraph.getAllEdges());
		validate(mst);
		return maxSpacing;
	}	
	
	public static int clusteringUsingUnionFind(String fileName,int thresholdDistance) {
		
		
		UnionFind unionFind = new UnionFind(200000);
		int [] distances = new int[] {0,1,2};
		HashMap<Integer,Set<int[]>> distanceToEdgesMapping = HammingDistanceUtility.readHammingDistanceFromFile(fileName,2);
		for (int distance:distances) {
			Set<int[]> edges = distanceToEdgesMapping.get(distance);
			for (int[] edge:edges) {
				int node1 = edge[0];
				int node2 = edge[1];
				if (unionFind.find(node1) != unionFind.find(node2)) {
					unionFind.union(node1, node2);
				}
			}
		}

		return unionFind.getClusterCount();
	}		
	
	public static void main(String args[]) {
		String fileName = "C:/Learning/Specialization-Algoriithms/Course3/Week2/clustering_big.txt";
		System.out.println( "Max spacing is " + clusteringUsingUnionFind(fileName,2) );
	}
	
	public static void validate(MinimumSpanningTree mst) {
		Set<Integer> nodes = new TreeSet<>();
		mst.getEdges().forEach(edgeDetail -> {
			nodes.add(edgeDetail[0]);
			nodes.add(edgeDetail[1]);
		});
		System.out.println(nodes.size());
	}

}
