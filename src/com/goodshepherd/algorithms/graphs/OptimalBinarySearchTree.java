package com.goodshepherd.algorithms.graphs;

public class OptimalBinarySearchTree {
	private int nodeCount = 0;
	private double [] nodeFrequencies = null;
	private double[][] weightedSearchCost = null;
	
	public OptimalBinarySearchTree(double[] nodeFrequencies) {
		this.nodeFrequencies = nodeFrequencies;
		this.nodeCount = nodeFrequencies.length;
		weightedSearchCost = new double[nodeCount][nodeCount];
		
		for (int i = 0; i < nodeCount;i++) {
			for (int j = 0; j < nodeCount;j++) {
				weightedSearchCost[i][j] = Double.MAX_VALUE;
			}
		}
	}
	
	public double getWeightedSearchCostAt(int i, int j) {
		System.out.println(String.format("i is %s and j is %s", i,j));
		if (i > j || i >= nodeCount || j >= nodeCount) {
			return 0;
		} else {
			return weightedSearchCost[i][j];
		}
	}
	
	public void setWeightedSearchCost(int i, int j,double cost) {
		System.out.println(String.format("i is %s and j is %s", i,j));
		if ( i < nodeCount && j < nodeCount) {
			weightedSearchCost[i][j] = cost;
		}
	}
	
	public double getNodeFrequency(int nodeNum) {
		if (nodeNum < nodeCount) {
			return nodeFrequencies[nodeNum];
		} else {
			return 0;
		}
	}
	
	public double getOptimalWeightedSearchCost() {
		for (int s = 0; s < nodeCount;s++) {
			for (int i = 0; i < nodeCount;i++) {
				double sumOfFrequencies = 0; 
				for (int r = i;r<=i+s;r++) {
					if (r >= nodeCount) {
						System.out.println(r);
					}
					sumOfFrequencies = sumOfFrequencies + getNodeFrequency(r);
					double currIterationCost = getWeightedSearchCostAt(i, r-1) + getWeightedSearchCostAt(r+1, i+s);
					if ( currIterationCost < getWeightedSearchCostAt(i,i+s)) {
						setWeightedSearchCost(i,i+s,currIterationCost);
					}
				}
				
				setWeightedSearchCost(i,i+s,getWeightedSearchCostAt(i,i+s) + sumOfFrequencies);
			}
		}
		
		return getWeightedSearchCostAt(0,nodeCount-1);
	}

	public static void main(String[] args) {
		//double [] nodeFrequencies = {0.05,0.4,0.08,0.04,0.1,0.1,0.23};
		double [] nodeFrequencies = {0.2,0.05,0.17,0.1,0.2,0.03,0.25};
		OptimalBinarySearchTree optimalBinarySearchTree = new OptimalBinarySearchTree(nodeFrequencies);
		System.out.println(optimalBinarySearchTree.getOptimalWeightedSearchCost());

	}

}
