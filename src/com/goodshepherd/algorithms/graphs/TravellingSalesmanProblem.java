package com.goodshepherd.algorithms.graphs;

import java.io.BufferedReader;
import java.io.FileReader;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

public class TravellingSalesmanProblem {
	
	public static final String FILE_NAME = "C:/Learning/Specialization-Algoriithms/Course4/Week2/tsp.txt";

	static class  AllowedNodeSet <T> {
		private int id;
		private Set<T> allowedNodes;
		
		public AllowedNodeSet(int id, Set<T> allowedNodes) {
			this.id = id;
			this.allowedNodes = allowedNodes;
		}
		
		@Override
		public String toString() {
			return new StringBuilder("Id:").append(id).append(", Allowed Nodes:").append(allowedNodes).toString();
		}
		
		@Override
		public boolean equals(Object obj) {
			if (obj == null) {
				return false;
			}
			
			if (! (obj instanceof AllowedNodeSet) ) {
				return false;
			}
			
			return this.id == ((AllowedNodeSet)obj).id;
		}
	}
	
	public static Map<Integer,Set< AllowedNodeSet<Integer>> > createNodesOfAllSizes(int numNodes) {
		int maxNum = (int)Math.pow(2d,(double)numNodes);
		Map<Integer, Set< AllowedNodeSet<Integer>> > map = new TreeMap<>();
		for (int i =0;i<maxNum;i++) {

			String binaryString = Integer.toBinaryString(i);
			//if (binaryString.endsWith("1")) {
				int node = binaryString.length();
				Set<Integer> allowedNodes = new TreeSet<>();
				for (int j = 0; j < binaryString.length();j++) {
					char ch = binaryString.charAt(j);
					if ( ch == '1') {
						allowedNodes.add(node);
					}
					node --;
				}
				
				
				int size = allowedNodes.size();
				if (size >= 1) {
					Set< TravellingSalesmanProblem.AllowedNodeSet<Integer> > listOfAllowedNodesOfSize = map.get(size);
					if (listOfAllowedNodesOfSize == null) {
						listOfAllowedNodesOfSize = new HashSet<>();
					}
					listOfAllowedNodesOfSize.add(new AllowedNodeSet<>(i, allowedNodes));
					map.put(size, listOfAllowedNodesOfSize);
				}
			//}
		}
		return map;
	}
	
	public static int getSetIdAfterRemovingNode(int setId, int nodeToRemove) {
		if (setId == 7 && nodeToRemove == 3) {
			System.out.println("Here");
		}
		String binaryString = Integer.toBinaryString(setId);
		//String newBinaryString = binaryString.substring(0,binaryString.length() - nodeToRemove) + "0" + binaryString.substring(binaryString.length() - nodeToRemove + 1);
		String newBinaryString = binaryString.substring(0,binaryString.length() - nodeToRemove) + "0" + binaryString.substring(binaryString.length() - nodeToRemove + 1);
		return Integer.parseInt(newBinaryString,2);
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		try (BufferedReader br = new BufferedReader(new FileReader(FILE_NAME))) {
			String lineRead = br.readLine();
			int numOfNodes = Integer.parseInt(lineRead);
			
			BigDecimal[] xCods = new BigDecimal[numOfNodes];
			BigDecimal[] yCods = new BigDecimal[numOfNodes];
			int idx = 0;
			while ( (lineRead = br.readLine()) != null) {
				String [] coordinates = lineRead.split(" ");
				xCods[idx] = new BigDecimal(coordinates[0]);
				yCods[idx] = new BigDecimal(coordinates[1]);
				idx++;
			}
			
			BigDecimal[][] edgeLengths = new BigDecimal[numOfNodes][numOfNodes];
			for (int k = 0; k < numOfNodes;k++) {
				for (int l = 0; l < numOfNodes;l++) {
					if (k == l) {
						edgeLengths[k][l] = BigDecimal.ZERO;
					} else {
						BigDecimal squaredDiff = (xCods[k].subtract(xCods[l])).pow(2).add( 
								((yCods[k].subtract(yCods[l])).pow(2))
								); 
						edgeLengths[k][l] = new BigDecimal(Math.sqrt(squaredDiff.doubleValue()));
					}
					//System.out.println(String.format("distance between node %s and %s is %s",k+1,l+1,edgeLengths[k][l]));
				}
			}
			
			int firstArrayLength = 12;
			int secondArrayLength = numOfNodes - (firstArrayLength-2);
			BigDecimal[] xCodsFirstSet = new BigDecimal[firstArrayLength];
			BigDecimal[] yCodsFirstSet = new BigDecimal[firstArrayLength];
			BigDecimal[] xCodsSecondSet = new BigDecimal[secondArrayLength];
			BigDecimal[] yCodsSecondSet = new BigDecimal[secondArrayLength];	
			System.arraycopy(xCods, 0, xCodsFirstSet, 0, firstArrayLength);
			System.arraycopy(yCods, 0, yCodsFirstSet, 0, firstArrayLength);
			System.arraycopy(xCods, (firstArrayLength-2), xCodsSecondSet, 0, secondArrayLength);
			System.arraycopy(yCods, (firstArrayLength-2), yCodsSecondSet, 0, secondArrayLength);
			
			
			System.out.println("xCodsFirstSet length " + xCodsFirstSet.length);
			System.out.println("yCodsSecondSet length " + yCodsSecondSet.length);
			System.out.println("Distance between node 11 and 12 is " + edgeLengths[10][11]);
			BigDecimal unnecessaryDistance = edgeLengths[10][11].multiply(new BigDecimal(2));
			
			double firstPathDistance = travellingSalesmanShortestPath(xCodsFirstSet,yCodsFirstSet);
			System.out.println(firstPathDistance);
			
			double secondPathDistance = travellingSalesmanShortestPath(xCodsSecondSet,yCodsSecondSet);
			System.out.println(secondPathDistance);
			
			double shortestPath = firstPathDistance + 
					secondPathDistance- unnecessaryDistance.doubleValue();
			
			System.out.println("Travelling Salesman Shortest Path is "+(int)shortestPath);
			
			//System.out.println(createNodesOfAllSizes(15));
			
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}
	
	public static double travellingSalesmanShortestPath(BigDecimal[] xCods,BigDecimal[] yCods) {
		
		double[][] edgeLengths = new double[xCods.length][xCods.length];
		for (int k = 0; k < xCods.length;k++) {
			for (int l = 0; l < xCods.length;l++) {
				if (k == l) {
					edgeLengths[k][l] = 0;
				} else {
					BigDecimal squaredDiff = (xCods[k].subtract(xCods[l])).pow(2).add( 
							((yCods[k].subtract(yCods[l])).pow(2))
							); 
					edgeLengths[k][l] = Math.sqrt(squaredDiff.doubleValue());
				}
				//System.out.println(String.format("distance between node %s and %s is %s",k+1,l+1,edgeLengths[k][l]));
			}
		}
		
		int numNodes = xCods.length;
		Map<Integer,Set< AllowedNodeSet<Integer>> > nodeSets = createNodesOfAllSizes(xCods.length);
		Map <Integer,Integer> nodeSetIdMap = new HashMap<>();
		int idx = 0;
		for ( Set< AllowedNodeSet<Integer>> allowedNodesSetsOfSize: nodeSets.values()) {
			for (AllowedNodeSet<Integer> alllowedNodeSet:allowedNodesSetsOfSize) {
				nodeSetIdMap.put(alllowedNodeSet.id,idx);
				idx++;
			}
		}
		
		double[][] distances = new double[idx][xCods.length];
		distances[0][0]= Double.POSITIVE_INFINITY;
		distances[1][0]=0;
		for (int a = 2;a<idx;a++) {
			distances[a][0] = Double.POSITIVE_INFINITY;
		}
		//System.out.println("nodeSetIdMap:"+nodeSetIdMap);
		for (int m = 2;m <=numNodes;m++) {
			Set< AllowedNodeSet<Integer>> allNodeSetsOfSizeM = nodeSets.get(m);
			for (AllowedNodeSet<Integer> aNodeSetOfSizeM:allNodeSetsOfSizeM) {
				Set<Integer> jNodes = aNodeSetOfSizeM.allowedNodes;
				
				for (Integer jNode:jNodes) {
					
					Set<Integer> otherNodes = new TreeSet<>(jNodes);
					otherNodes.remove(jNode);
					otherNodes.remove(1);
					double smallestDistance = 0d;
					if (otherNodes.size() == 0 ) {
						smallestDistance = edgeLengths[0][jNode -1];
					}
					
					for (Integer kNode:otherNodes) {
						int setId = getSetIdAfterRemovingNode(aNodeSetOfSizeM.id,jNode);
						int setIdx = nodeSetIdMap.get(setId);
						
						if (smallestDistance == 0d) {
							smallestDistance = distances[setIdx][kNode-1] + edgeLengths[kNode-1][jNode -1];
						} else {
							smallestDistance = Math.min(smallestDistance, distances[setIdx][kNode-1])+ edgeLengths[kNode-1][jNode -1];
						}
						
					}
					distances[nodeSetIdMap.get(aNodeSetOfSizeM.id)][jNode-1] = smallestDistance;
				}
			}
		}
		
		int maxId = (int)Math.pow(2d,(double)numNodes);
		double shortestDistance = 0d;
		for (int j = 2;j<=numNodes;j++) {
			if (shortestDistance == 0) {
				shortestDistance = distances[nodeSetIdMap.get(maxId-1)][j-1] + edgeLengths[j-1][0];
			} else {
				shortestDistance = Math.min(shortestDistance, distances[nodeSetIdMap.get(maxId-1)][j-1] + edgeLengths[j-1][0]);
			}
		}
		return shortestDistance;
	}

}
