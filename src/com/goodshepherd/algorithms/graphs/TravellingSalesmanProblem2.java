package com.goodshepherd.algorithms.graphs;

import java.io.BufferedReader;
import java.io.FileReader;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.stream.Collectors;

public class TravellingSalesmanProblem2 {
	
	public static final String FILE_NAME = "/home/gshepherd/projects/algorithms-java/src/resources/tsp.txt";
	//"C:/Users/workpc/eclipse-workspace/Algorithms/src/resources/testcase1.txt";

	static class  AllowedNodeSet <T> implements Comparable<T>{
		private int id;
		private Set<T> allowedNodes;
		
		public AllowedNodeSet(int id, Set<T> allowedNodes) {
			this.id = id;
			this.allowedNodes = allowedNodes;
		}
		
		@Override
		public String toString() {
			return new StringBuilder("Id:").append(id).append(", Allowed Nodes:").append(allowedNodes).toString();
		}
		
		@Override
		public int hashCode() {
			return id;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			AllowedNodeSet other = (AllowedNodeSet) obj;
			if (id != other.id)
				return false;
			return true;
		}
		
		public int compareTo(T o) {
			return this.hashCode() - o.hashCode();
		}
		
	}
	
	public static List <AllowedNodeSet<Integer>>  createNodesOfAllSizes(int numNodes) {
		int maxNum = (int)Math.pow(2d,(double)numNodes);
		//Map<Integer, List< AllowedNodeSet<Integer>> > map = new TreeMap<>();
		List <AllowedNodeSet<Integer>> allNodesSet = new ArrayList<>();
		for (int i =0;i<maxNum;i++) {

			String binaryString = Integer.toBinaryString(i);
			//if (binaryString.endsWith("1")) {
				int node = binaryString.length();
				Set<Integer> allowedNodes = new HashSet<>();
				for (int j = 0; j < binaryString.length();j++) {
					char ch = binaryString.charAt(j);
					if ( ch == '1') {
						allowedNodes.add(node);
					}
					node --;
				}
				if (allowedNodes.contains(1)) {
					allNodesSet.add(new AllowedNodeSet<>(i, allowedNodes));
				}
				/*
				int size = allowedNodes.size();
				if (size > 0) {
					List< TravellingSalesmanProblem2.AllowedNodeSet<Integer> > listOfAllowedNodesOfSize = map.get(size);
					if (listOfAllowedNodesOfSize == null) {
						listOfAllowedNodesOfSize = new ArrayList<>();
					}
					listOfAllowedNodesOfSize.add(new AllowedNodeSet<>(i, allowedNodes));
					map.put(size, listOfAllowedNodesOfSize);
				}
				*/
		}
		return allNodesSet;
	}
	
	public static int getSetIdAfterRemovingNode(int setId, int nodeToRemove) {
		String binaryString = Integer.toBinaryString(setId);
		String chStr = binaryString.substring(binaryString.length() - nodeToRemove,binaryString.length() - nodeToRemove +1);
		if ("0".equals(chStr)) {
			return -1;
		} else {
			String newBinaryString = binaryString.substring(0,binaryString.length() - nodeToRemove) + "0" + binaryString.substring(binaryString.length() - nodeToRemove + 1);
			//String newBinaryString = binaryString.substring(0,binaryString.length() - nodeToRemove)  + binaryString.substring(binaryString.length() - nodeToRemove + 1);
			return Integer.parseInt(newBinaryString,2);
		}
	}
	
	
	public static void trial() {
		int numNodes = 4;
		double[][] edgeLengths = new double[numNodes][numNodes];
		for (int i = 0; i < 4;i++) {
			edgeLengths[i][i] = 0;
		}
		
		edgeLengths[0][1] = 1;
		edgeLengths[1][0] = 1;
		edgeLengths[1][2] = 6;
		edgeLengths[2][1] = 6;
		edgeLengths[2][3] = 5;
		edgeLengths[3][2] = 5;
		edgeLengths[0][3] = 2;
		edgeLengths[3][0] = 2;
		edgeLengths[0][2] = 3;
		edgeLengths[2][0] = 3;
		edgeLengths[1][3] = 4;
		edgeLengths[3][1] = 4;
		double shortestPath = travellingSalesmanShortestPath(edgeLengths, numNodes);
		System.out.println("Shortest path is "+ shortestPath);
	}
	
	public static void allInOneShot() {
		try (BufferedReader br = new BufferedReader(new FileReader(FILE_NAME))) {
			String lineRead = br.readLine();
			int numOfNodes = Integer.parseInt(lineRead);
			
			double[] xCods = new double[numOfNodes];
			double[] yCods = new double[numOfNodes];
			int idx = 0;
			while ( (lineRead = br.readLine()) != null) {
				String [] coordinates = lineRead.split(" ");
				xCods[idx] = Double.parseDouble(coordinates[0]);
				yCods[idx] = Double.parseDouble(coordinates[1]);
				idx++;
			}
			
			double[][] edgeLengths = computeEdgeLengths(xCods, yCods);
			
			double shortestPathDistance = travellingSalesmanShortestPath(edgeLengths,numOfNodes);
			
			System.out.println("Shortest path distance is "+ (int)shortestPathDistance);
			
		} catch (Exception ex) {
			ex.printStackTrace();
		}


	}
	
	public static void iterative() {
		// TODO Auto-generated method stub
//		trial();
//		System.exit(-1);
		
		try (BufferedReader br = new BufferedReader(new FileReader(FILE_NAME))) {
			String lineRead = br.readLine();
			int numOfNodes = Integer.parseInt(lineRead);
			
			double[] xCods = new double[numOfNodes];
			double[] yCods = new double[numOfNodes];
			int idx = 0;
			while ( (lineRead = br.readLine()) != null) {
				String [] coordinates = lineRead.split(" ");
				xCods[idx] = Double.parseDouble(coordinates[0]);
				yCods[idx] = Double.parseDouble(coordinates[1]);
				idx++;
			}
			
			double[][] edgeLengths = computeEdgeLengths(xCods, yCods);
			
			
			Map<Integer,Integer> shortestPathList = new TreeMap<>();
			Map<Integer,Integer> shortestPathListWithoutRemovalOnce = new TreeMap<>();
			Map<Integer,Integer> shortestPathListWithoutRemovalTwice = new TreeMap<>();
			
			for (int i = 12;i <=12;i++) {
				int firstArrayLength = i;
				int secondArrayLength = numOfNodes - (firstArrayLength-2);
				double[] xCodsFirstSet = new double[firstArrayLength];
				double[] yCodsFirstSet = new double[firstArrayLength];
				double[] xCodsSecondSet = new double[secondArrayLength];
				double[] yCodsSecondSet = new double[secondArrayLength];	
				System.arraycopy(xCods, 0, xCodsFirstSet, 0, firstArrayLength);
				System.arraycopy(yCods, 0, yCodsFirstSet, 0, firstArrayLength);
				System.arraycopy(xCods, (firstArrayLength-2), xCodsSecondSet, 0, secondArrayLength);
				System.arraycopy(yCods, (firstArrayLength-2), yCodsSecondSet, 0, secondArrayLength);
				
				
//				System.out.println("xCodsFirstSet length " + xCodsFirstSet.length);
//				System.out.println("yCodsSecondSet length " + yCodsSecondSet.length);
				System.out.println(String.format("Distance between node %s and %s is ",i-1,i) + edgeLengths[i-2][i-1]);
				double unnecessaryDistance = edgeLengths[i-2][i-1];
				
				double [][] edgeLengthsFirstSet = computeEdgeLengths(xCodsFirstSet, yCodsFirstSet);
				double firstPathDistance = travellingSalesmanShortestPath(edgeLengthsFirstSet,yCodsFirstSet.length);
				//System.out.println(firstPathDistance);
				
				double [][] edgeLengthsSecondSet = computeEdgeLengths(xCodsSecondSet, yCodsSecondSet);
				double secondPathDistance = travellingSalesmanShortestPath(edgeLengthsSecondSet,yCodsSecondSet.length);
				//System.out.println(secondPathDistance);
				
				double shortestPath = firstPathDistance + 
						secondPathDistance- unnecessaryDistance;
				
				shortestPathList.put((int)(firstPathDistance + secondPathDistance), i);
				shortestPathListWithoutRemovalOnce.put((int)(firstPathDistance + secondPathDistance-unnecessaryDistance*1), i);
				shortestPathListWithoutRemovalTwice.put((int)(firstPathDistance + secondPathDistance-unnecessaryDistance *2), i);
			}
			
			
			
			System.out.println("Shortest path list without deletion is "+ shortestPathList);
			System.out.println("Shortest path list with deletion times 1 is "+ shortestPathListWithoutRemovalOnce);
			System.out.println("Shortest path list with deletion times 2 is "+ shortestPathListWithoutRemovalTwice);
			
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}
	
	public static double[][] computeEdgeLengths(double [] xCods, double[] yCods) {
		double[][] edgeLengths = new double[xCods.length][xCods.length];
		for (int k = 0; k < xCods.length;k++) {
			for (int l = 0; l < xCods.length;l++) {
				edgeLengths[k][l] = Math.pow(
						Math.pow((xCods[k] - xCods[l]),2) + Math.pow((yCods[k] - yCods[l]),2),
						0.5);
				
			}
		}
		
		return edgeLengths;
		
	}
	
	public static void main(String[] args) {
		Instant start = Instant.now();
		//trial();
		allInOneShot();
		//iterative();
		System.out.println("Compeltion Time in millis is " + Duration.between(start, Instant.now()).toMillis());
	}
	
	
	
	
	public static double travellingSalesmanShortestPath(double[][] edgeLengths,int numNodes) {
		int selectedNodePath = 0;
		
		List <AllowedNodeSet<Integer>>  nodeSets = createNodesOfAllSizes(numNodes);
		
		System.out.println("Done creating nodes of size:"+ nodeSets.size());

		int maxNodeId = (int)(Math.pow(2d,(double)numNodes) -1);
		/*
		System.out.println("Max node set id:" + maxNodeId);
		System.out.println("Idx corresponding to Max node set id:" + nodeSetIdMap.get(maxNodeId));
		int maxKey = nodeSetIdMap.keySet().stream().max( (i1,i2) -> i1- i2).get();
		System.out.println( "nodeSetIdMap maxKey:" + maxKey );
		System.out.println("nodeSetIdMap idx from maxKey:" + nodeSetIdMap.get(maxKey));
		*/
		
		double[][] distances = new double[maxNodeId+1][numNodes];
		
		for (int a = 0;a<=maxNodeId;a++) {
			if (a==1) {
				distances[a][0]=0;
			} else {
				distances[a][0] = Double.POSITIVE_INFINITY;
			}
			
		}
		//System.out.println("nodeSetIdMap:"+nodeSetIdMap);
		for (AllowedNodeSet<Integer> aNodeSet:nodeSets) {
				if (aNodeSet.allowedNodes.contains(1)) {
					Set<Integer> jNodes = aNodeSet.allowedNodes;
					if (aNodeSet.id == 255) {
						System.out.println("255");
					}
					
					//double smallestDistance = Double.POSITIVE_INFINITY;
					for (Integer jNode:jNodes) {
												
						if (jNode != 1) {
							Set<Integer> otherNodes = new HashSet<>(jNodes);
							otherNodes.remove(jNode);
							otherNodes.remove(new Integer(1));
							int setId = getSetIdAfterRemovingNode(aNodeSet.id,jNode);
							
							double smallestDistance = Double.POSITIVE_INFINITY;
							
							if (distances[aNodeSet.id][jNode-1] > 0) {
								//smallestDistance = distances[aNodeSet.id][jNode-1];
							}
							
							if (otherNodes.size() == 0 ) {
								smallestDistance = Math.min(smallestDistance, edgeLengths[0][jNode -1]);
							} else {
								for (Integer kNode:otherNodes) {
									smallestDistance = Math.min(smallestDistance, distances[setId][kNode-1]+ edgeLengths[kNode-1][jNode -1]);
								}
							}
							//distances[nodeSetIdMap.get(aNodeSetOfSizeM.id)][jNode-1] = smallestDistance;
							distances[aNodeSet.id][jNode-1] = smallestDistance;
						}
						
					}
				}
		}
		
		for (int i = 0 ; i < numNodes;i++) {
			System.out.println(String.format("distances[%s][%s] = %s",maxNodeId,i,distances[maxNodeId][i]));
		}
		
		double shortestDistance = Double.POSITIVE_INFINITY;
		for (int j = 1;j<numNodes;j++) {
			if (distances[maxNodeId][j] + edgeLengths[j][0] < shortestDistance) {
				selectedNodePath = j;
			}
			double dist = distances[maxNodeId][j] + edgeLengths[j][0];
			shortestDistance = Math.min(shortestDistance, dist);
			System.out.println(String.format("With node %s the distance is %s and distance back to node 1 is %s",j+1,dist,edgeLengths[j][0]));
		}
		System.out.println("selectedNodePath:"+selectedNodePath);
		return shortestDistance;
	}

}
