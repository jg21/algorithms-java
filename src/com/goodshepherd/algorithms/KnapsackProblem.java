package com.goodshepherd.algorithms;

import java.io.BufferedReader;
import java.io.FileReader;

public class KnapsackProblem {
	
	int capacity;
	int [] elementWeights;
	int [] elementValues;
	int numOfElements;
	long [][] optimalValueAtCapactity;
	public static final String FILE_NAME = "C:/Learning/Specialization-Algoriithms/Course3/Week4/knapsack1.txt";
	
	public KnapsackProblem(int capacity, int [] elementWeights, int [] elementValues) {
		this.capacity = capacity;
		this.elementWeights = elementWeights;
		this.elementValues = elementValues;
		numOfElements = elementValues.length;
		optimalValueAtCapactity = new long [numOfElements][capacity+1];
	}
	
	public long optimalKnapsackSolution() {
		
		for (int i = 0; i < numOfElements;i++) {
			int elementWeight = elementWeights[i];
			int elementValue = elementValues[i];
			for ( int j = 0; j <= capacity; j ++) {
				if (i == 0) {
					optimalValueAtCapactity[i][j] = 0;
				} else {
					long a = getOptimalValueAtCapacity(i-1, j);
					if (j == 249) {
						System.out.println("Stop here");
					}
					long b = getOptimalValueAtCapacity(i-1, j - (elementWeight) )+elementValue;
					if (elementWeight > j) {
						optimalValueAtCapactity[i][j] = a;
					} else {
						optimalValueAtCapactity[i][j] = Math.max(a, b);
					}
					
				}
			}
		}
		
		return optimalValueAtCapactity[numOfElements-1][capacity];
	}
	
	public long getOptimalValueAtCapacity(int i, int j) {
		if (i < 0 || j < 0) {
			return 0;
		}
		
		return optimalValueAtCapactity[i][j];
	}

	public static void main(String[] args) {
		try (BufferedReader br = new BufferedReader(new FileReader(FILE_NAME))) {
			String lineRead = br.readLine();
			String[] firstLine = lineRead.split(" ");
			int capacity = Integer.parseInt(firstLine[0]);
			int numOfElements = Integer.parseInt(firstLine[1]);
			int [] elementWeights = new int[numOfElements+1];
			int [] elementValues = new int[numOfElements+1];
			elementValues[0] = 0;
			elementWeights[0] = 0;
			int i = 1;
			while ( (lineRead = br.readLine()) != null ) {
				String [] lineContents = lineRead.split(" ");
				elementValues[i] = Integer.parseInt(lineContents[0]);
				elementWeights[i] = Integer.parseInt(lineContents[1]);
				i++;
			}
			
			KnapsackProblem knapsackProblem = new KnapsackProblem(capacity, elementWeights, elementValues);
			System.out.println(knapsackProblem.optimalKnapsackSolution());
			

		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

}
