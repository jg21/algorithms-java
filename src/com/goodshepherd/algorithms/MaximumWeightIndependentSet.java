package com.goodshepherd.algorithms;

import java.io.BufferedReader;
import java.io.FileReader;

public class MaximumWeightIndependentSet {
	public static final String FILE_NAME = "C:/Learning/Specialization-Algoriithms/Course3/Week3/mwis.txt";
	
	static class MaxWeightSet {
		private int[] maxWeights = null;
		private boolean[] isInMaxWtIndSet;
	}
	
	public static MaxWeightSet retrieveMaximumWeightIndependentSet(String fileName) {
		
		MaxWeightSet maxWeightSet = new MaxWeightSet();
		
		try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
			String lineRead = br.readLine();
			int numOfVertexes = Integer.parseInt(lineRead);
			int[] vertexWeights = new int[numOfVertexes];
			int[] maxWeights = new int[numOfVertexes];
			boolean[] isInMaxWtIndSet = new boolean[numOfVertexes];
			maxWeightSet.isInMaxWtIndSet = isInMaxWtIndSet;
			maxWeightSet.maxWeights = maxWeights;
			int i = 0;
			while ( (lineRead = br.readLine()) != null ) {
				vertexWeights[i] = Integer.parseInt(lineRead);
				if (i == 0) {
					maxWeights[i] = vertexWeights[i];
				} else if (i == 1) {
					maxWeights[i] = Math.max(vertexWeights[i-1], vertexWeights[i]);
				} else {
					maxWeights[i] = Math.max(maxWeights[i-2] + vertexWeights[i], maxWeights[i-1]);
				} 
				i++;
			}
			
			i = i -1;
			
			while(i > 1){
				if (maxWeights[i-1] > maxWeights[i-2] + vertexWeights[i]) {
					i--;
				} else {
					isInMaxWtIndSet[i] = true;
					i = i -2;
				}
				
			}
			
			if (isInMaxWtIndSet[2]) {
				isInMaxWtIndSet[0] = true;
			} else {
				isInMaxWtIndSet[1] = true;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			
		}
		
		return maxWeightSet;
		
	}

	public static void main(String[] args) {
		MaxWeightSet maxWeightSet = retrieveMaximumWeightIndependentSet(FILE_NAME);
		
		int[] vertexList = new int[] {1, 2, 3, 4, 17, 117, 517,997};
		StringBuilder sb = new StringBuilder();
		for (int i = 0;i < vertexList.length;i++) {
			sb.append(maxWeightSet.isInMaxWtIndSet[vertexList[i]-1] ? "1":"0");
		}
		
		System.out.println(sb.toString());

	}

}
